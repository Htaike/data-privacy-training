# Title
--- meta
title:
uuid:
lan: en
author:
item:
tags:
duration:
description:
---

## Meta information

### Description
This a short description and overview of the activity.


### Duration
The length of time for this activity. Remember that this activity is only part of a SLE which should not exceed 1 hour 30 minutes. In some cases an activity can be the main part of a SLE. Like for example a Focused Group Discussion (FGD).


### Ideal Number of Participants
Indicate here the ideal number of participants for this activity. Some activities are require more participants to make it work. For example a World Cafe activity, requires a minimum 9 participants, divided into 3 groups of 3 participants per group.


### Objective/s
Every activity should support a specific objective of the SLE where it will be applied. It can be a single and /or combination or all of the learning objectives below:

Knowledge (K):
- What specific ideas, facts, insights do we want the participants to learn.

Skill (S):
- What specific know-how and ability do we want the participants to gain. This is more related to parts of the SLE that has to do with more practical items like learning a specific application.

Attitude (A):
- What specific bias and beliefs we want to challenge and propose alternatives.


### Materials and Equipment Needed
--- meta
uuid:
tag: materials activity
---

Indicate here the necessary materials and equipment needed to make the activity work. Find the UUID numbers for the materials under Materials.md

Example:
- :[Flip chart](417fc8a5-400f-4553-a23e-faa949beb239) : []
- :[Markers](9392dacf-999c-4c33-a6d8-4545c1aee849) : []
- :[Post-its](c0358b51-fe16-47ae-9686-927ec39d18f6) : []


### References
Indicate here relevant reference documents, links and the like for this activity. It is always nice to attribute individuals, organizations, and websites :)


## Activity: <name of activity>
--- meta
uuid:
---
Indicated here the step-by-step instructions for this activity, divided between preperation and steps during the activity. It's a good idea to indicate the following as part of the steps:

Preparation:
- What needs to be prepared before this activity

Activity:
- Introduction to the activity and it's relevance to the SLE.
- Step by step walk through activity
-
-




-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
