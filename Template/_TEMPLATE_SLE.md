# Structured Learning Experiences (SLE)
--- meta
title:
uuid:
lan:
author:
item:
tags:
duration:
description:
dependencies:
---

## Meta information

### Workshop Description
This is the general assessment of our audiences' environment and the learning gap that we have identified and need to address.

This should be complimented by a Training Needs Assessment (TNA) depending on the specific target participants and group/s. This allows us to focus and identify the peculiarities of the target participants and the current environment they work in.

*note: We need to draft a TNA document.

### Overview

### Duration
Ideally LSEs should not exceed 1 hour 30 minutes. If the SLE requires a longer time it can be divided into chunks of 1 hour 30 minutes of related sessions.


### Ideal Number of Participants
The number  of participants largely depend on the number of facilitators. To be consistent with our "How-To: Organise a Training" document, below is s the ideal participant to facilitator ratio:

1 Trainer  :  8 Participants
2 Trainers : 20 Participants
4 Trainers : 28 Participants


### Objectives
Every SLE should identify (if applicable), three learning objectives that cover the areas of knowledge, skill and attitude.

Knowledge (K):
- What specific ideas, facts, insights do we want the participants to learn.
Skill (S):
- What specific know-how and ability do we want the participants to gain. This is more related to parts of the SLE that has to do with more practical items like learning a specific application.
Attitude (A):
- What specific bias and beliefs we want to challenge and propose alternatives.

This are the same items that we need to measure at the end of the SLE.

*note: We need to draft an evaluation document with measurable metrics to determine the success of SLEs.


### Materials and Equipment Needed
--- meta
uuid:
tag:
---
Example of referencing materials
- @[Beamer](ce457811-1423-4ff0-93bb-7bc2fda1e844) : []
- @[Workshop Material](67c7149b-44f9-4b7a-b4ee-c8bf786b50dc) : []

Indicate here the necessary materials and equipment that this SLE would require. Some of the materials and equipment are also listed in the related Activity and Ice Breaker document.


### References
Indicate here relevant reference documents, links and the like for this SLE. It is always nice to attribute individuals, organizations, and websites :)


## Steps
Indicate here the flow and timings of the SLE. Important things to consider if applicable:

- Participant introduction that gets everybody talking and moving early on the session.

- Brief presentation of the SLE.

- Participants' expectation check.

- Discuss, modify, and agree on the learning objectives.

- Identify Activities relevant to the SLE.

- Identify Ice Breakers for this SLE.

- Ensure that an evaluation happens at the end of the SLE.

Example of referencing a specific activity
### Step 2 brainstorm:
---
uuid: >>> refer to the specific uuid under steps of an activity
Activity: Post-it Frenzy
Tag:
---

<!---
curriculum-training/SLE/TEMPLATE
-->
