# Boost Your Browser (Hands-on)
--- meta
title: Boost Your Browser - Hands-on
uuid: aa20266a-501e-4490-bbea-ad5902fcbc75
lan: en
author: Tactical Tech
item: SLE
tags:
  - Browser
  - Hands-on
duration:
description: How does tracking in the browser work? Find out, and then take steps to limit the information your browser gives away.
dependencies:
---

## Meta information

### Description
How does tracking in the browser work? Find out, and then take steps to limit the information your browser gives away.

### Overview
1. Introductions (10 min)
2. _Optional_: See what data is collected through the browser (20 min)
3. Privacy and tracking (30 min)
4. Hands-on: Change default settings and install key add-ons (30 mins)

### Duration
90 - 120 minutes


### Ideal Number of Participants
Since this is a hands-on session, there should be a minimum of two facilitators, ideally with a third facilitator to step in where needed (see _How-To Organise a Training_).

- 2-20 participants: 2-3 trainers
- 20-28 participants: 4 trainers

### Objectives
##### Knowledge
- Understand how tracking works, specifically: what data is collected, by whom, and how, where, when, and why (for what pruposes)
- Know which add-ons and extensions to use, and which privacy settings are important.

##### Skills
- Be able to make informed decisions about how you share your data.
- Know how to change default settings and install add-ons and extensions.  


##### Attitude
- How your browser is configured can change your internet experience and determine how much information others can see about your browsing habits.
- There are simple ways to make your browsing more private and more secure.


### Materials and Equipment Needed
--- meta
uuid: 182f25f3-1264-4f46-bfcf-990f40a75247
tag: SLE materials
---
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](67c7149b-44f9-4b7a-b4ee-c8bf786b50dc)
- @[material](ce39f7f2-0357-419a-aca7-81806e00e6cf)
- @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)

### References
- REFERENCE DOCUMENT FOR MOBILES
- [Install Firefox](https://www.mozilla.org/en-US/firefox/new/)
- [Prevent Online Tracking](https://myshadow.org/prevent-online-tracking), Me and My Shadow (Tactical Tech)
- [How to control your data on Firefox](https://myshadow.org/how-to-increase-your-privacy-on-firefox), Me and My Shadow (Tactical Tech)
- [How to control your data on Chrome](https://myshadow.org/how-to-increase-your-privacy-on-chrome)
- [Browser Security Vulnerability Review](https://secunia.com/resources/vulnerability-review/browser-security/) (Secunia Research)
-[Panopticlick](https://myshadow.org/panopticlick) (EFF)
- [Firefox and security add-ons for Windows](https://securityinabox.org/en/guide/firefox/windows), Security in-a-box (Tactical Tech)
- [Firefox and security add-ons for Mac OS-X](https://securityinabox.org/en/guide/firefox/os-x), Security in-a-box (Tactical Tech)
- [Firefox and security add-ons for Linux](https://securityinabox.org/en/guide/firefox/linux), Security in-a-box (Tactical Tech)
- [Track Me Not](https://myshadow.org/track-me-not) (add-on).
- [Do Not Track](https://cryptoparty.at/_media/graz:2015-04-24_anton_do-not-track.pdf) (pdf)
- [Webtracking](https://cryptoparty.at/_media/graz:2015-04-25_anton_bernhard_webtracking_p-8-correct.pdf)


## Steps
*NOTE: This workshops requires the REFERENCE DOCUMENT FOR MOBILES*

### Step 1: Introductions (10 min)
Briefly introduce yourself and the session, then ask participants to introduce themselves and answer the following questions:  
- What browsers do you use, and what do you use these for?
- What do you want to learn in this session?

Taking expectations into account, give a brief overview of the session, including objectives, what will be covered (and what not), and how much time is available.

### Step 2: (Optional) See what data is collected through the browser (20 min)
The following activity is designed to show what types of data traces you leave behind when you browse the web. It only needs to be done if the group has not already done a session on "Browser Basics".

Activity: Visualising Tracking in the Browser
@[activity](ea61792c-218f-48a0-8a9c-47fa611f07d3)

### Step 3: Privacy and tracking (30 min)
Lead a group discussion on tracking. Involved participants by eliciting answers before giving explanations. Topics to cover:

##### Cookies
- What does it mean to disable cookies?
- What does it mean to talk about "good" cookies vs. "bad" cookies?

##### Tracking
- What is a "Do not track" request?
- What's the difference between privacy settings, "Do not track" and anti-tracking add-ons like Privacy Badger?

##### Browser History
- Why is it good to not have your browser save your browsing history?

##### Search engines
- What search engines do you use?
- What search engines are out there, and what's the difference between them? Why would you choose, say, DuckDuckGo over Google Search?
- How can you change your default search engine?

##### Incognito Mode & Private Browsing Mode
- What are Incognito Mode (Chrome) & Private Browsing Mode (Firefox)?
- What are the differences between these vs. changing the default settings manually?

#### Step 4: Hands-on: Install Firefox, change default settings and install key add-ons (60 min)
For this Hands-on Session, walk participants through:
- Installing the Firefox browser
- Adjusting settings
- Installing key add-ons.
Use the Firefox How-To on myshadow.org as a guide.

Notes on how to Run a Hands-on Session:
@[activity](ab6c7bf6-330c-428b-99c8-03aad4552ddf)


### Step 5: Wrap up - evaluation quiz (20 min)
- This quiz can be done all together, or it can be printed out and done individually or in small groups.
- Go through the answers together and then answer any remaining questions.  

Find the Quiz here: https://myshadow.org/materials


-------------------------------
<!---
curriculum-training/SLEs/TEMPLATE
-->
