# Secure Messaging Apps for Mobile (Hands On)
--- meta
title: Secure Messaging Apps for Mobile (Hands-on)
uuid: 0769e3a2-898e-4fd3-9f1f-c32294bd8b4c
lan: en
author: Tactical Tech
item: SLE
tags:
  - Mobile
  - Secure messaging
  - Choosing tools
  - Hands-on
duration: 50
description: Which messaging apps you use? How are apps different, and how do you make the best choice? Why does it matter? Learn how to evaluate your apps, and then and install and use Signal Secure Messenger.  
---

## Meta information

### Description
Which messaging apps do you use? How are apps different, and how do you make the best choice? Why does it matter? Learn how to evaluate your apps, and then and install and use Signal Secure Messenger.       

### Overview
1. Introductions (10 min)
2. Choosing messaging apps (20 min)
3. Hands-on: Installing Signal (15 min)
4. Wrap up (10 min)


### Duration
55 minutes


### Ideal Number of Participants
This session splits up participants into two groups according to their operating systems, iOS or Android, so there should be a minimum of two facilitators, with a third facilitator to step in where needed (see _How-To Organise a Training_).

- 2-20 participants: 2-3 trainers
- 20-28 participants: 4 trainers

### Objectives
##### Knowledge
- Know how to evaluate tools and services generally, through key concepts like open source, ownership, and owner-company location.
- Understand the main differences between 'mainstream' and 'alternative' tools and services, and between selected messaging apps in each of these categories.
- Understand the basics of encryption: how it works, and why it is important.  

##### Skills
- Be able to make more informed decisions about the messaging apps you use.
- Be able to install and use Signal for encrypted instant messaging and encrypted voice calls.  

##### Attitude
- As a user, you have to choose what information you want to protect. Do you want to manage your identity, social network, content or location? Protecting one might expose the other.
- Contributing to a free, decentralised, neutral internet is in the hands of everyone. We can and should contribute to the sustainability and resilience of alternatives.
- It is possible to have private communications through the encryption of mobile communications data.


### Materials and Equipment Needed
--- meta
uuid: 525102fb-974f-4fd5-95f7-40baf47b5269
tag: SLE materials
---
- @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
- @[material](6ebfc3d0-3f8a-41b9-800b-5e802b985fd8)


### References
- [Signal for Android](https://securityinabox.org/en/guide/signal/android), Security in-a-box (Tactical Tech)
- [Signal](https://myshadow.org/signal), Me and My Shadow (Tactical Tech)
- [Open Whisper Systems](https://whispersystems.org/), the non-profit developers of Signal.


## Steps
*IMPORTANT*: This workshop requires the REFERENCE DOCUMENT FOR MOBILES.

### Step 1: Introductions (10 min)
Briefly introduce yourself and the session, then ask participants to introduce themselves and answer the following questions:  
- What messaging apps do you use, and why do you use these specific apps?
- What do you want to learn in this session?

Taking expectations into account, give a brief overview, including objectives, what will be covered (and what not), and how much time is available.

### Step 2: Choosing messaging apps (20 min)
@[activity](bc9d5aee-0106-41dd-9164-7f2157e16260)

### Step 3: Hands-On: Installing Signal (15 min)
Split participants into two groups, depending on their operating system (Android or iPhone). Each group should have at least one facilitator.

1. Install the Signal app directly from the Play Store or  App Store (participants need to have an App Store account with either Google or Apple to be able to do this)
2. Exchange contacts with fellow participants in each group, and verify each other.
3. Start exchanging encrypted instant messages and voice calls with fellow participants
4. Participants can choose whether they want to configure Signal to be their default messaging app or not.


### Step 4: Wrap up (10 min)
- See if anything is still unclear, and answer questions
- Direct participants to resources
- Hand out Tactical Tech's Pocket Privacy guide for Mobiles if you have them.


-------------------------------
<!---
curriculum-training/Exhibition/TEMPLATE
-->
