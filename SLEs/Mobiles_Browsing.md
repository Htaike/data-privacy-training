# Browsing on your Mobile (Hands-on)
--- meta
title: Browsing on your Mobile (Hands-on)
uuid: 8c82c04d-2566-4081-954b-030db1031ba1
lan: en
author: Tactical Tech
item: SLE
tags:
  - Mobile
  - Browser
  - Hands-on
duration: 205
description: See what gets shared with others through your browser, and how. Then take concrete steps to make your web browsing more private and secure.
dependencies:
---

### Meta information

### Workshop Description
See what gets shared with others through your browser, and how. Then take concrete steps to make your web browsing more private and secure.

### Overview
1. Introductions (10 min)
2. The Browser is a two-way street (60 min)
3. Browsing on your mobile: Hands-on  (60 min)
4. Search engines (20 min)
5. Strategize (30 min)
6. Wrap up (15 min)


### Duration
3h05 (not including breaks)

### Ideal Number of Participants
This session splits up participants into two groups according to their operating systems, iOS or Android, so there should be a mimimum of two facilitators, ideally with a third facilitator to step in where needed (see _How-To Organise a Training_).

- 2-20 participants: 2-3 trainers
- 20-28 participants: 4 trainers

### Objectives
##### Knowledge
- See how and why your browser might not be as secure as you might think, and where insecurities lie.
- Understand what 'encryption' means, how the internet works (infrastructure), and how and why online tracking fuels advertising.
- Know what is meant by 'anonymity' online, and understand the basics of Tor.

##### Skill
- Be able to make informed decisions about which browsers to use
- Be able to limit data traces left through the browser
- Learn how to: *Android*:
 - Install Firefox browser, adjust settings to limit tracking, and install HTTPS Everywhere.
 - Install and use Orbot and Orfox
- Learn how to: *iPhone*:
 - Set permissions in Safari to limit tracking.

##### Attitude
- Choices can be made and simple actions taken to limit tracking.


### Materials and Equipment Needed
--- meta
uuid: ce4f68ad-6c20-4552-87d0-c1679cb1682f
tag: SLE materials
---
- @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
- @[material](6ebfc3d0-3f8a-41b9-800b-5e802b985fd8)


### References
- REFERENCE DOCUMENT FOR MOBILE

## Steps
*IMPORTANT*: This workshop requires the REFERENCE DOCUMENT FOR MOBILE.

### Step 1: Introductions (10 min)
Briefly introduce yourself and the session, then ask participants to introduce themselves and to say what they want to learn in this session.

Taking expectations into account, give a brief overview of the session, including objectives, what will be covered (and what not), and how much time is available.


### Step 2: The browser is a two-way street (60 min)
While the browser allows us to access the internet, it also allows others to access lots of information about _us_.

The following activity gives an overview of how the internet works, showing shows what information third parties can see when you browse the web or send email. It then shows what others can see when you use the internet via https and via Tor.
@[activity](9a32d927-f292-4834-939a-320fa96df94d)


#### Tracking (30 min)
Tracking in the browser is often invisible. The following activity gives participants the chance to see how tracking works.
@[activity](ea61792c-218f-48a0-8a9c-47fa611f07d3)


### Step 3: Browsing on your mobile: Hands- On Session (60 min)
Run a hands-on session that looks at the following (find detailed information in the **REFERENCE DOCUMENT FOR MOBILES**)

#### Android
- **Compare browsers** for Android (Firefox, Chrome, and Orfox)
- **Install and customise** Firefox* (On Android, adjusting settings is not possible in other browers)
- **Install Orfox and Orbot*
- Discuss **alternative app stores** and help participants configure their phone settings to accept apps from other sources if they want to use these alternatives.

#### iPhone
- **Compare browsers** for iPhone (Safari, Firefox, and Chrome).
- **Customise Safari** (no other browser can be customised on iPhone)
- **Discuss VPNs** Since Orfox and Orbot are not available for iPhone, a VPN is the next best option. Show participants how to install a VPN and install if possible.

@[activity](ab6c7bf6-330c-428b-99c8-03aad4552ddf)


### Step 4: Search engines (20 min)
Use the following activity to help participants understand important differences between commercial and 'alternative' tools, with a focus on search engines.

@[activity](bc9d5aee-0106-41dd-9164-7f2157e16260)

### Step 5: Strategize (30 min)
Use the following activity to guide participants through key strategies for taking more control of the data they share with commercial companies. They will already have covered some of the material, so adapt it as fits the needs of the group.

@[activity](1667798e-fb3d-4ae6-a744-ddc6ae44bdf8)


### Step 6: Wrap up (15 min)
- See if anything is unclear, and answer questions
- Direct participants to resources
- Hand out Tactical Tech's Pocket Privacy guide for Mobiles if you have them.


-------------------------------
<!---
curriculum-training/Exhibition/TEMPLATE
-->
