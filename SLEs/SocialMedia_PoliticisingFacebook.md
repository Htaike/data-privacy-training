# Politicising Facebook
--- meta
title: Politicising Facebook
uuid: a79790b4-9ef0-45fb-8230-1843ceba686b
lan: en
author: Tactical Tech
item: SLE
tags:
  - Social media
  - Debate  
duration: 90
description: A critical, discussion-based look at Facebook from a social and political perspective, and a closer examination of how we interact with this company.
---
## Meta information

### Workshop Description
A critical, discussion-based look at Facebook from a social and political perspective, and a look at how we ourselves interact with this company.  


### Overview

### Duration
90 minutes

### Ideal Number of Participants
Minimum of 8.

### Workshop Objectives
##### Knowledge
- Understand central social, cultural and political issues around Facebook

##### Skill
- Be able to construct and use logical arguments in discussion

##### Attitude
- Social Media is occupying more and more space in the political sphere and public debate. We tend to forget that those platform are privately owned and are subject to the rules, control and interests of the corporations that own them.


### Materials and Equipment Needed
---
uuid: 4a67871b-ec16-4a67-816b-15cb00ac387a
tag: SLE materials
---
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)

### References
Facebook, Me and My Shadow (Tactical Tech)

### Preparation
The workshop requires participants to debate political statements about Facebook. Option A involves crowd-sourcing statements from the participants; Option B requires you to provide the group with statements. Even if you take Option A, you should still prepare some statements as back-up - ideally, contextualised to the group or region.  

##### Example statements
- Facebook has compelling reasons to censor - to limit profanity and hate speech.
- Facebook should be the one setting the rules for what happens on the platform; not users or the government.
- Facebook enables violence towards women who express their opinions.  


## Steps

### Step 1: Introductions (10 min)
Briefly introduce yourself and the session, then ask participants to introduce themselves and say what they hope to learn in this session.

Taking expectations into account, give a brief overview, including objectives, what will be covered (and what not), and how much time is available.

### Step 2 (optional): Crowd-source statements about Facebook (10 min)
- Break the group into smaller groups
- Ask each group to write down, on post-its, as many statements about Facebook that they can think of (One statement per post-it). For context, ask them to think about the centralisation of power and data, living in a "Post-Privacy world", etc.
- Each group should then choose one statement.

#### Feedback
- Bring the groups back together, and ask each group to read out their statement. These will be used for the following activity.


### Step 3: Lager House Debate A (60 min)
Use the chosen post-it statements from Step 1, or use your back-up statements to hold a lager-house discussion.

@[activity](6d8b622b-de50-4dc6-8696-1b2bfde78dec)

### Step 4: Lager House Debate B (60 min)
_This step can be used as either an alternative to, or a follow on from, the previous debate._

Use a Lager House format to have the group discuss the following statement:

"Facebook is a Public Space".

@[activity](6d8b622b-de50-4dc6-8696-1b2bfde78dec)

In the follow-up discussion, ask the group some of the following:
- Are social media platforms public or private spaces? How does the general public view them?
- Who has control over Facebook and what messages are we are allowed to deliver?
- What is an algorithm? How do algorithms affect the public debate? Any famous cases?  
- Does censorship have a legitimate place in how Facebook operates? Collaboration with governments?
- Collection of data is a concern since extensive profiles can be drawn from the data shared on Facebook. Who has access to this data? Do we, or should we, trust a corporation with that?
- cases where Facebook has manipulated public opinion?


### Step 5: Wrap up (10 min)
- See if anything is still unclear, and answer questions
- Direct participants to resources

-------------------------------
<!---
curriculum-training/SLEs/TEMPLATE
-->
