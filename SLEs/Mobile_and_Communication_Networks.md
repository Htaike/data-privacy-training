# Mobile Communication
--- meta
title: Mobile Communication
uuid: be6287dd-2a2f-4679-8cd1-6771c481e3ea
lan: en
author: Tactical Tech
item: SLE
tags:
  - Mobile
  - Drawing exercise
duration: 130
description: How does mobile communication work? Who has access to your information along the way? And what you can do to take back some control over what information others have access to?
---
## Meta information

### Workshop Description
How does mobile communication work? Who has access to your information along the way? And what you can do to take back some control over what information others have access to?

### Overview
1. Introductions (10 min)
2. How mobile communication works (45 min)
3. Your phone: A breakdown in four layers (45 min)
4. What can you do? Tips on managing your traces (20 min)
5. Consolidation quiz (10 min)
6. Wrap up: Questions, clarifications and resources (10 min)

### Duration
2h10

### Ideal Number of Participants
15-20.

(For guidance on participant-trainer ratios, see _How-To Organise a Training_)


### Objectives
##### Knowledge
- Understand the basics of mobile technology: how a mobile phone and mobile communications infrastructure work, and what data traces you leave behind.
- Understand the trade-offs and basic security issues related to Android, iPhone, Windows and Feature phones respectively.


### Materials and Equipment Needed
--- meta
uuid: 35008b9e-0627-4d53-8cd8-00843040c586
tag: SLE materials
---
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](a1108a3a-7bd8-4afc-9255-02a181ccffef)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)
- @[material](16c01d17-9ba7-47d6-815a-75cf9633004f)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](1d01a081-f4a9-408b-b06b-78ee6288d1ce)

### References
- _Mobile Tools_, Security in-a-box (Tactical Tech) https://securityinabox.org/en/mobile-tools
- _Trainers' Curriculum_, Mobile Safety (Level Up) https://www.level-up.cc/curriculum/mobile-safety/
- _Umbrella_ app for Android </a> (Security First) https://play.google.com/store/apps/details?id=org.secfirst.umbrella&hl=en


## Steps
*IMPORTANT*: This workshop requires the REFERENCE DOCUMENT FOR MOBILE.

### Step 1: Introductions (10 min)
Briefly introduce yourself and the session, then ask participants to introduce themselves and answer the following questions:
- What do you use your phone for?
- What do you want to learn in this session?

Taking expectations into account, give a brief overview of the session, including objectives, what will be covered (and what not), and how much time is available.

### Step 2: The basics of mobile communication (45 min)
The following activity allows participants to see how mobile communication actually works, and what information third parties have access to along the way.

@[activity](541f7a7b-e83f-4018-8646-b86fcbccca4)


### Step 3: How does a phone work? What information is shared? (40 min)
The following activity looks at how a mobile phone works, and how different parts of the mobile phone are used for information collection and tracking.
@[activity](be34b4fa-dbc9-4b15-aa3e-b9354ddf5ee8)

### Step 4: What can you do? Tips on managing your traces (20 min)
Walk participants through the **General Tips** section of the _Mobiles Reference Document_.


### Step 6: Mobile Communication: Consolidation Quiz (10 min)
Download the Mobile Communication Quiz from https://myshadow.org/materials. The quiz can be done using a projector or by reading out the questions, or it can be done individually with printed-out forms. 

This can be done individually or in groups.

```
1. Describe three ways in which your location can be tracked through your mobile.
.............................................................................................
.............................................................................................
.............................................................................................
.............................................................................................

2. Who owns your baseband?
- \[ \] Telecommunication provider
- \[ \] Google
- \[ \] Apple
- \[ \] HTC

3. Name five data traces you can think of that are created by automatically by the mobile infrastructure and hardware.
.............................................................................................
.............................................................................................
.............................................................................................
.............................................................................................
 4. Name five data traces that are created voluntarily.

 .............................................................................................
 .............................................................................................
 .............................................................................................
 .............................................................................................

5. What is an operating system and what are the main differences between Android, iOS and Windows?
.............................................................................................
.............................................................................................
.............................................................................................
.............................................................................................
```

### Step 06: Wrap up (10 min)
- See if anything is unclear, and answer questions
- Direct participants to resources
- Hand out Tactical Tech's Pocket Privacy guide for Mobiles if you have them.


-------------------------------
<!---
curriculum-training/SLEs/TEMPLATE
-->
