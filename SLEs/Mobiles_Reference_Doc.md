# Mobiles Reference Document
---
title: Mobiles Reference Document
uuid: 1194bf77-6e7a-4974-8e24-22c0a85111bb
lan: en
author: Tactical Tech
item: SLE
tags:
- Mobile
- Reference document
---

## Meta information

### Description
Essential reference document for all workshops and activities related to mobile phones and mobile communication.

### Supporting resources and references
- [Mobile Tools](https://securityinabox.org/en/mobile-tools), Security in-a-box, _Tactical Tech._
- [Trainers' Curriculum: Mobile Safety](https://www.level-up.cc/leading-trainings/training-curriculum/mobile-phones), _Level Up._
- [Umbrella app for Android](https://play.google.com/store/apps/details?id=org.secfirst.umbrella&hl=en), _Security First_

## Steps
### Contents
1. How different phones are different: comparing phones
2. Criteria for a secure phone
3. Connecting to the internet:
- How the internet works
- http and https
- Tor
4. Browsers for mobile

### How different phones are different: Comparing phones
Each type of phone has a specific set of advantages and disadvantages. Below is a comparison of Android phones, iPhones, and Windows phones, and feature phones.

#### Android
###### +
- Android operating system is open source
- An open source app store can be used: F-Droid
- Security tools available: Psiphon, Panic button
- Open source secure communication tools available: Signal, Chatsecure
- Can connect to the Tor network through tools Orbot and Orfox
- Multiple layers of defence from malware
- Device manager for lost and stolen phones

###### -
- Google's business model based on use of data
- Despite defences, there is malware (eg Androrat)

_Notes_
- Not all android versions are the same in terms of features and security.

#### iPhone

###### +
- A strong screen lock can be enabled
- There is less malware in the app store/iTunes
- Permissions are given separately for each app
- Open source, encrypted communication tools available: Signal, Chatsecure, Surespot
- Find my iPhone for lost and stolen phones

###### -
- Closed source
- Not many security apps available, compared to Android.
- Default screen lock is only 4 digits
- Encryption keys are stored on apple servers
- There are encryption tools in the App Store but it's closed source. ???


#### Windows Phone
###### -
- Closed source
- No Security apps available
- No encryption

#### Feature phone
###### -
- No Encryption
- Closed source
- Usually no screen lock

### Secure phones: Criteria
If you're looking to buy a phone that you can make as secure as possible, here are some criteria to consider:

- Battery: The battery should be removable
- Updates: The phone's manufacturer should have a good history of pushing updates regularly.
- Operating System: Check the version - does it allow for encrypting the phone? (Older versions don't)
- Chip set: The Media Tech Chip (MTK) set is the only chip that allows you to change the IMEI number. [Resource for devices with this chip](https://en.wikipedia.org/wiki/List_of_devices_using_Mediatek_SoCs)

### Connecting to the internet
#### How the internet works
EFF has a good interactive diagram showing the infrastructure of the internet:
https://www.eff.org/pages/tor-and-https

The diagram also shows who can see what along the way, and shows how this changes when you connect to the internet via HTTPS or when you use Tor.

#### HTTP and HTTPS
- When you connect to a website, the connection between you and the website will use one of two protocols: HTTP (the default), or HTTPS.
 - When you connect using **HTTPS**, this created an encrypted connection between the website and your mobile phone. This means that any data you send over the connection (emails, passwords and so on) is scrambled and can't be deciphered by anyone along the way.
 - Without HTTPS, ie when a connection is made using just **HTTP**, our data, including any passwords, travels  unencrypted across the web, and can be seen by third parties along the way.
 - You can see your connection is using HTTPS by the green lock to the left of the URL. If the green lock is not there, your data is travelling in the clear.
- HTTPS is most consistently used in online banking, email, and shopping websites, though many other websites support HTTPS as well. Whether a website supports an HTTPS connection in the first place is up to the website owner, who needs to implement an HTTPS certificate.
- Sometimes a website will support HTTPS, but not connect in this way by default. In Firefox for Android, you can install an add-on called HTTPS Everywhere to force connections over HTTPS where possible.

##### HTTPS Everywhere
- _**Android:**_ To force connections over HTTPS where the option exists (ie, where the website supports https), you can install a simple add-on in the Firefox browser called _HTTPS Everywhere_.  This by default encrypts your connections to all websites that support HTTPS. https://www.eff.org/https-everywhere
- _**iPhone:**_ HTTPS Everywhere is not available for Firefox on iPhone.


#### Tor
https://www.torproject.org/
- Connecting to the internet via the Tor network allows users to hide their IP address, while simultaneously blocking trackers and enforcing HTTPS where available. This allows people to be anonymous online.
- **Android** To use Tor, Android users need to install **Orbot**, which connects users to the Tor network, and then install the **Orfox** browser. https://guardianproject.info/ (creators of Orbot and Orfox)
- Tor cannot be used on an iPhone.
- **Using Tor: Legality & red flags:** In some countries, use of Tor can raise a red flag, and is illegal in some countries. Depending on your own assessment of your risk Tor might not be a good option for you.
- **Countering the myth that "Tor is only used by criminals:"**
This is just not true, as Tor is also used by activist, dissidents, and many other types of people. 
() it is true that criminals do use Tor, it is also true that criminals use the ordinary internet, the streets we all walk on, etc.  Crime exists because of other, socio-political problems, not because of software.

#### VPNS

### Browsers
#### Android
Browsers available for Android:
Firefox, Chrome, Orfox (_Note that all these browsers can be run over Orbot._).

##### Firefox
Developed by: Mozilla

##### +
- On Android, Firefox is the only browser where you can change the default privacy settings and install add-ons.
- Has a proven commitment to security, with highly-paid bug bounty programmes
- Handles SSL certification revocation better than any other browser
- Data protection is a key issue: corporate manifesto states, “individuals' security and privacy on the Internet are fundamental and must not be treated as optional”
- The source code is available (Firefox is the only browser that is fully open source)
- Developed by Mozilla, a non-profit organisation that produces free, quality software. This means that Firefox is not being used as part of larger profit-making agenda
- Has an easy to use privacy mode, but the privacy settings can also be customised


##### -
 - Security still not as high as Chrome.


##### Chrome
Developed by: Google

##### +
- Security: Chrome is among the best browsers out there.
- Flash is built in and automatically updated, which means that vulnerabilities are kept to a minimum (note however that security studies of Chrome were funded by Google in 2011, and a lot has changed since then.)
- Has an easy-to-use Incognito Mode; also allows for privacy settings to be customised.

##### -
- Privacy: Google is an advertising company and makes the majority of its money through advertising. For this they use information they collect through their services, including Chrome, to find out what you do, where you are, what you buy, etc.
- On Android, users are not able to change default settings
- Not open source.

##### Orfox
Developed by: The Guardian Project

##### +
- Privacy: Orfox by default runs over Tor, and therefor offers online anonymity (hides your IP address), and blocks online tracking.
- Circumvention: helps users circumvent online censorship.

##### -
- Might not be able to use all online services

##### Note
Orfox requires prior installation of Orbot to function.

- **Note:** Install both Orbot and Orfox directly from Google Play, F-droid or from the Guardian Project.

_More information / Resources_
 - **Orbot: Hands-on Guide** https://securityinabox.org/en/guide/orbot/android
 - **Orfox: Hands-on-Guide** https://guardianproject.info/2015/06/30/orfox-aspiring-to-bring-tor-browser-to-android/


### Browsers: iPhone
Browsers available for iPhone:
Safari, Firefox, Chrome.

##### Safari
Developed by: Apple

_Benefits:_
 - Safari is the only browser on iPhone where you can change the privacy settings

 - **Customise Safari (iPhone)**
   - change search engine
   - block pop-ups
   - Do No Track
   - Delete history and website data.
   - The browser history is stored under Settings > Safari > advanced > Websitedata


##### Firefox
Developed by: Mozilla

_Benefits:_
- Has a proven commitment to security, with highly-paid bug bounty programmes
- Handles SSL certification revocation better than any other browser
- Data protection is a key issue: manifesto states: “individuals' security and privacy on the Internet are fundamental and must not be treated as optional.”
- Source code is available (Firefox is the only browser that is fully open source)
- Developer Mozilla is a non-profit organisation that produces free, quality software. This means that Firefox is not being used as part of larger profit-making agenda
- Has an easy-to-use Private Browsing mode, which includes tracking protection.

_Limitations:_
 - Security still not as high as Chrome.
 - Firefox on iPhone is the Safari browser wrapped into a Firefox shell. Therefore, there are no add-ons available.
 - Default settings can't be changed.

##### Chrome
Developed by: Google

_Benefits:_
 - Security: Chrome is among the best browsers out there.
 - Flash is built in and automatically updated, which means that vulnerabilities are kept to a minimum (note however that security studies of Chrome were funded by Google in 2011, and a lot has changed since then.)
 - Has an easy-to-use Incognito Mode.

_Limitations:_
 - Privacy: Google is an advertising company and makes the majority of its money through advertising. For this they use information they collect through their services, including Chrome, to find out what you do, where you are, what you buy, etc.
 - Default settings can't be changed.
 - Not open source.

### Search Engines
_Privacy-friendly alternatives to Google Search or Bing:_
- DuckDuckGo: https://myshadow.org/duckduckgo (also available as a mobile app)
- Searx: https://myshadow.org/searx
- Ixquick: https://myshadow.org/ixquick
- StartPage: https://myshadow.org/startpage

DuckDuckGo also exists as a mobile app.

### Encrypting your phone
- While newer iPhones are encrypted by default, Android users need to encrypt their phones manually.

- Be aware that the process encrypting your phone requires a password; and **if you forget this password, you will be locked out of your phone permanently**.

- So, before encrypting:
  - Back up important data on your phone (Contact list, Photos, Videos, etc).
  - Charge your phone so it doesn't break the encryption process.
  - Use strong password to encrypt the phone, and make sure that you can remember this password.


### Location tracking
Location tracking works in three main ways:
  - Wi-Fi: When you use the internet via Wi-Fi, your location can be tracked IP address.
  - Mobile data: Your phone is constantly connecting to cell phone towers in the vicinity, each of which has a location. Triangulation allows your exact location to be pinpointed.
  - GPS: Your phone's  built-in GPS allows for accurate tracking (by either the US or Russia, depending on what type of GPS your phone has)

#### "Find my phone" / "Anti theft" apps?
  Should you use an app that helps you locate your phone?

  If your phone is lost or stolen, a 'find my phone' app can be used to:
  - block the phone
  - find the phone if it is lost
  - make the phone ring or make a noise
  - erasing the data on the phone.

  Whether you decide to use one of these apps will depend on your answers to the following questions:
  - Is this solution actually useful to you? What is the risk of your phone being stolen?
  - If your phone was stolen, what would the impact be?
  - Are you prepared to put in a bit of work initially? (downloading the app, registering the phone, learning how it works, testing it)
  - Do you trust the app developer with the data they will subsequently have access to?

  If you decide to install a 'find my phone' app, **Avira** is a good one. http://www.avira.com/



### Mobiles: Hands-on checklist
- **Email address:**  Your phone always has a primary email address connected to it, which is also connected to your App/Play Store account. If you create a new email address just for your phone, this can limit social graphing and can help to compartmentalise your digital traces. Though not possible on Android, it's preferable not to use a gmail account.

- **Phone "name":** Give your phone a name that is not connected to your identity. If someone scans the network, this is the name they will see.

- **Password:** Put a strong password on your phone.

- **Wi-Fi:** Turn off Wi-Fi when you're not using it. When Wi-Fi is on, it continuously broadcasts your Wi-Fi history to Wi-Fi networks in the vicinity.

- **Bluetooth:** Turn off bluetooth when you're not using it.

- **Limit location tracking**: To limit location tracking, turn off your phone and take out the battery if possible. If you want to be 100% sure, just leave your phone at home.

- **Change the way you connect to the internet**: Set up a VPN (can be set up on Android or iPhone) or Tor (can only be used on Android, via Orfox and Orbot) to connect to the internet.

- **Install Orbot and Orfox (Android)**
To use Orfox, you first need to install Orbot. This enables you to connect to the Tor network.

 - **Orbot**: Install Orbot directly from Google Play, F-droid or from the Guardian Project's app store. You are now connected to the Tor network.

  - **Orfox**: Once Orbot is installed, install Orfox directly from Google Play, F-droid or from the Guardian Project app store. You can now browse the web through your mobile phone anonymously.

- **Choose a Browser:**
 - Compare the browsers available for your operating system and choose a default browser.
 - Note: Tor is not available for the iPhone, for safer browsing it is recommended to use a VPN and route traffic through this.

- **Customise browser settings: Firefox (Android)**
  - Change the search engine
  - Block pop-ups
  - Do Not Track
  - Delete history and website data.
  - Install HTTPS Everwhere  

- **Customise browser settings: Safari (iPhone)**
 - Safari is the only browser you can change settings in. To customise Safari:
  - change search engine
  - block pop-ups
  - Do No Track
  - Delete history and website data.
  - The browser history is stored under Settings > Safari > advanced > Websitedata


- **Apps: limit access/permissions**
  - **Control access:** Though apps are "verified" by store owners (Google or Apple), in reality this provides weak protection against what applications will do after being installed on your phone - some applications may, for example, copy and send out your address book. You can often, to an extent, control what an app is allowed to do, and what other parts of your phone it has access to.

  - **Permissions**: Check what kind of access your installed apps have to the following: Location, Contacts, Photos, Calender, Speakers, Microphone, Camera.

  Limit access permissions where possible. If an app requires you to give more  access than you're comfortable with, consider deleting the app.  Perhaps a better alternative exists, or perhaps you don't really need that app after all.
   - _**Android:**_ Each application needs to request permissions during the installation process.
   - _**iPhone**_ You can check and manage app permissions in the settings.

  - **Necessary access:**  Sometimes certain access is required for the app to function - for example, many messenger apps (like Whatsapp and Signal) require access to your Contacts list. Whether or not you are comfortable with this app having this kind of permission should come into your decision about whether or not to use the app.

  - **Over-reach** Does your newsreader app have access to your contacts? Does the city navigation app you just downloaded want access to your photos? Why? Since these permissions are not central to the app's functioning, they can often be refused. If not, you should consider alternative applications which request more appropriate access and rights.

  - Keep in mind that because installing an app requires being logged in to either the App Store or the Play Store, either Google or Apple have a record of what you've searched for in the store, and what you've downloaded and installed.

### Alternative app stores
##### Android
- If you want to opt out of Google's Play Store, there are alternatives - for example** F-Droid** or the **Guardian Project's app store**, both of which only offer free and open source apps. In general, you should trust a site before you download any apps from it.

- To install tools from an app store other than Google Play, you need to configure your phone settings accordingly.

##### iPhone
- It is not possible to opt out of Apple's App Store.

#### Verifying whether an app is legitimate**
- You can verify apps through their gpg signature. By default, Android requires all applications to be signed. In basic terms, this means that the application's signature is used to identify the author of an application (i.e. verify its legitimacy), as well as establish trust relationships between applications with the same signature.

- **_Example:_**
 This is the signature of the orbot application on F-Droid:
 https://f-droid.org/repo/org.torproject.android_15012316.apk.asc

 This is the signature of the orbot application on Google play
 ADD

- One of the easiest ways to verify a signature is to download the app several times, from several locations, several computers, and several websites and then compare all signatures to make sure that they are all identical and belong to the same group of developers.
- Another way which is a bit more difficult, is by comparing gpg signatures. Here's a guide on how to do this:
 https://www.gnupg.org/download/integrity_check.html

- **Chekey**, an app by the Guardian Project, can also help you verify apps:
 https://play.google.com/store/apps/details?id=info.guardianproject.checkey


### Messenger apps

 LINK TO FORM DOWNLOAD



### Rooting / Jailbreaking your phone
Should you root your Android phone, or jailbreak your iPhone? The short answer is _Probably Not_ :)

 _Rooting_ means that you have root access to your device. It is like running programmes as adminstrators in Windows PCs.

 Rooting also means unlocking your operating system, which allows you to remove any built-in apps, and to install an alternative operating system, or apps that are not approved by Google/Apple or the phone's manufacturer.

_The good about rooting/jailbreaking:_
 - You can remove built-in apps that you don't use, to save space and battery.
 - If you are using Android 5 or below, you can install an application that will control app permissions.
 - You can install custom ROM to get the latest updates from Android, often delayed or blocked by the phone's manufacturer.
  - You gain full control over the privacy and security settings of the phone


_The bad about rooting/jailbreaking:_
 - You risk making your smartphone permanently inoperable ('bricking' it, i.e. turning it into a 'brick').
 - The manufacturer or mobile carrier warranty may be voided.
 - The process may not be  reversible.

 - _Note:_ In some places, rooting/jailbreaking is illegal.

Taking all of the above into account, if you do want to root your phone, these resources offer guidance:

 - http://www.cyanogenmod.org/
 - https://wiki.cyanogenmod.org/w/P760_Info
 - https://en.wikipedia.org/wiki/Replicant_(operating_system)

- Be aware that many rooting tools come from China, and a lot contain malware, so it's important to use a tool that can be trusted, like Superuser (available from F-droid):  https://f-droid.org/repository/browse/?fdcategory=Security&fdid=me.phh.superuser&fdpage=2
