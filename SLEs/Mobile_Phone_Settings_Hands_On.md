# Mobile Phone Settings (Hands-on)
--- meta
title: Mobile Phone Settings (Hands-on)
uuid: 6bb57df8-8fbc-49eb-9a43-0069c4320a57
lan: en
author: Tactical Tech
item: SLE
tags:
  - Mobile
  - Hands-on
duration: 90
description: Prevent others from accessing what's on your phone, by connecting to networks more securely, increasing the basic security of your phone, managing app permissions, using more privacy-friendly browsers and apps, and installing some key add-ons.
dependencies:
  - description: Mobile Browsing blah
    uuid: be6287dd-2a2f-4679-8cd1-6771c481e3ea
---
## Meta information

### Workshop Description
Prevent others from accessing what's on your phone, by connecting to networks more securely, increasing the basic security of your phone, managing app permissions, using more privacy-friendly browsers and apps, and installing some key add-ons.

### Overview
1. Introductions (10 min)

Hands-on Android, iPhone and feature phone
2. Negotiating access to the infrastructure
3. Security basics
4. Limiting permissions of apps
5. Safer browsing on a mobile
6. Evaluate applications

### Duration
90 minutes


### Ideal Number of Participants
This session splits up participants into two groups according to their operating systems, iOS or Android, so there should be a mimimum of two facilitators -  ideally with a third facilitator to step in where needed (see _How-To Organise a Training_).

2-20 participants: 2-3 trainers
20-28 participants: 4 trainers

### Objectives
##### Knowledge
Know which types of data are collected by default through mobile phones.
##### Skill
- Be able to increase the security of your mobile phone.
- Be able to configure a phone's settings to limit default data collection.
##### Attitude Mobile phones are by default largely insecure, but that there are some simple ways to increase control over your data.


### Materials and Equipment Needed
--- meta
uuid: fa68caf6-10f1-4718-9cbe-3399cc766328
tag: SLE materials
---

- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)



### References
- _How to: Android_ and _How to: iPhone_, Me and My Shadow (Tactical Tech)
- Android: basic setup, Security in a Box (Tactical Tech) https://securityinabox.org/en/guide/basic-setup/android
- How to encrypt your iphone, Surveillance Self-Defence guide (EFF) https://ssd.eff.org/en/module/how-encrypt-your-iphone


## Steps
NOTE: NEED TO CROSS-CHECK THIS AGAINST MOBILE REF DOC, AND

### Step 1: Introductions (10 min)
Briefly introduce yourself and the session, then ask participants to introduce themselves and to say what they hope to learn in the session.

Taking expectations into account, give a brief overview, including session objectives, what will be covered (and what not), and how much time is available.


### Step 2. Why is your phone insecure? (x min)
Walk the group through the following points:

- Mobile phones are to a large extent insecure by default. The infrastructure of mobile communication means that certain types of data are created automatically - the most significant of which is fairly precise location tracking.

- In addition to data that is created automatically by the phone (e.g. location logs), many more data traces are created when we use the phone and the applications on it (e.g. maps, facebook, email, and apps).

- Many of these traces are stored on the phone itself, which could have implications if your phone gets into the hands of others. Many data traces are also, however, collected by companies and institutions without your active consent.

- The good news is that you do have _some_ control over what data is created and collected. Privacy and digital security are not an all-or-nothing situation: every small step matters. The important thing is to get started and make it work for you.


### Step 03. Hands-on (x min)
Ask participants to take out their mobile phones and split them into two groups: Android & iPhone.

@[activity](ab6c7bf6-330c-428b-99c8-03aad4552ddf)


#### Hands-on: Android
WE NEED TO CROSS-CHECK THIS AGAINST THE MOBILES REFERENCE DOC AND SEE WHAT'S MISSING OR WHAT COULD BE PULLED FROM THERE....

Walk participants through the following.

###### 1. Limit access
- Turn off wifi, bluetooth, put in flight mode.
- **Location tracking:** Can you take out the battery of your phone? If not, to avoid your location being tracked you need to leave your phone at home, or use a faraday bag/cage.
- **Browsing the internet** Access the internet anonymously by installing Tor through the Orbot app
- VPN (select recommended VPN, configure in Wireless & Networks settings, participants can configure a VPN).


###### 2. Security basics
NOT ALL OF THESE ARE SECURITY BASICS - UNKNOWN SOURCES FOR EXAMPLE - HK
- **Passwords:**
  - Create strong password for your phone
  - Create passwords for individual apps
  - Disable passwords from being visible

- **Encryption:** Encrypt your phone (Show participants where they can do this at home. It is not recommended to do this during the session, as they might forget their password)

- **SIM card lock:** Set this up

- **Unknown sources:** Enable the installation of apps from unknown sources (this enables you to download apps from outside the Google Play Store)

- **Backup & reset:** You have the option to NOT have your data backed up on Google servers. Additionally, under _Factory data reset_ , you can reset your phones and erase all data from the phones' internal storage.

- **Unique numbers:** Know where to find the unique numbers of your phone (IMEI, SIM etc.). These can be useful if your phone is stolen.

- **Email account:** Create a new primary email account for your phone (this is the account that is tied to the Play Store). Android requires this email to be a Gmail account; however creating a new email address that is only tied to your phone / Google Play account and nothing else, makes it harder for Google to tie your different social networks together.

- **Phone "name"**: Change the name of your phone if necessary (from "Bobs Phone" to something less tied in with your identity)

- **Anti-virus:** Install Avira (It will also allow you to locate your phone if lost.)

- **Find my phone:** Anti-virus Avira has this capability; or download Android Device Manager from the Play Store.


###### 3: Limit permissions on a per-app basis

- On newer versions of Android you can set permissions per application. Consider restricting access to:
  - location
  - contacts
  - photos
  - microphone
  - camera
  - calender
- Note: on older versions of Android you can not set access for individual application you can only change the generic permission in the Google Settings sections on Android. _(See Step 4 below)._

###### 4. Limit Overall Permissions
- **Activity control:** Google Settings > Account > Personal info & privacy > Activity controls: Turn the following _OFF_:
  - YouTube Search History
  - YouTube Watch History
  - Google Location History.

- **Opt out of profile-based advertising:** Google Settings > Services > Ads: enable _Opt out of interest-based ads_. This will instruct your apps not to use your advertising ID to build profiles or show you interest-based ads. You can subsequently reset your advertising ID by tapping _Reset advertising ID_.

- **Location:** Google Settings > Services > Location: Here you can disable the location tracking option. Note that this will disable some services, such as certain features in Google Maps.

- **Delete location history:** Google Settings > Services > Location > Google Location History: Here you can disable and delete your Google location history.

###### 5. Safer browsing
- **Choose a browser:** Firefox is a non-profit browser with a stated commitment to protecting and respecting your private information, and is the only browser that allows you to change the settings.

- **Firefox: Set browser permissions**
  - Change the default search engine to one that respects your privacy (For example DuckDuckGo, a non-profit search engine which doesn't track its users).
  - Block pop-ups, enable Do No Track, delete history and website data.

- **Install Tor browser for Android, _Orfox_** (note: Orbot needs to be installed for Orfox to work)

###### 6. Evaluate your apps
- **Reduce:** Do you use all the applications on your phone? Which ones can be deleted?
- **Choose apps that respect your privacy**: Look at the apps on your phone. Why did you choose to install these specific apps? Was it because your friends asked you? Was it an active choice? Discuss what makes an app more secure and better for your privacy (open source, use of encryption, who owns it, where is the company located, what data does it request permission to, does it provide the option of pseudonymous or anonymous use? More information on https://myshadow.org)


#### Hands on: iPhone

Walk participants through the following:

###### 1. Limit access
- On iPhones, you can't take out the battery.To prevent your location being tracked you need to either leave your phone at home, or use a faraday bag/cage.
- When not in use, turn off wifi, bluetooth, put phone in flight mode
- Use a **VPN** to access the internet. SAY MORE HERE??

###### 2. Security basics
- **Password:** Protect your phone with a strong password. CAN YOU CREATE A LONGER ONE ON IPHONE???
- Password for apps: (find what to use to do this on iPhone)
- **Unique numbers:** Know where to find the unique numbers of your phone (IMEI, SIM etc.). These can be useful if your phone is stolen.
-  **Email account:** Create a new primary email account for your phone (this is the account that is tied to the App Store).
 Preferably not Google, as you want to separate which companies have access to your data.
- **Phone "name"**: Change the name of your phone if necessary (from "Bobs Phone" to something less tied in with your identity)
- **Anti-virus:** Install an anti-virus (IS THIS CORRECT? HK)
- **Find my phone:** Anti-virus Avira has this capability, or use Apple's _Find My iPhone_ tool.(see relevant section in Reference Doc for things to be aware of.)   
- **Back up and reset** ADD TO THIS

###### 3: Limit Permissions
- Settings > Privacy > restrict access per application for the following:
  - location
  - contacts
  - photos
  - microphone
  - camera
  - calender

###### 4. Safer browsing
- Compare three browsers for iPhone: Safari, Firefox, Chrome
- Safari is the only browser that allows you to change the settings.

- **Safari: Set browser permissions**

  - **Settings** > Safari >  Block pop-ups, Do No Track, Clear history and website data.
To find your browser history go to Settings > Safari > Under advanced > Websitedata
 - **Change the default search engine** to one that respects your privacy (for example DuckDuckGo, a non-profit search engine which doesn't track its users).


###### 6. Evaluate your apps
 - **Reduce:** Do you use all the applications on your phone? Which ones can be deleted?
 - **Choose apps that respect your privacy**: Look at the apps on your phone. Why did you choose to install these specific apps? Was it because your friends asked you? Was it an active choice? Discuss what makes an app more secure and better for your privacy (open source, use of encryption, who owns it, where is the company located, what data does it request permission to, does it provide the option of pseudonymous or anonymous use? More information on https://myshadow.org)


#### Hands on: Feature phone

A feature phone is a basic phone which doesn't have access to internet.

- Use a pin code
- Use a password if your phone allows for it
- Take the battery out to protect from location tracking and communication interception.


### Step 4: Evaluate learning

To see what participants have learned from this session try and collectively fill out the Strategies of Resistance for the mobile phone. Ask what you can do to (1.) Reduce (2.) Fortify (3.) Compartimetalize (4.) Obfuscate information on a mobile phone. Use a flipchart to write down the answers in a matrix.

Trainers notes:

- Reduce: Turning of services when not in use (wifi, bluetooth, put in flight mode), delete application no longer in use, change permission what data an app has access to
- Fortify: full disk encryption, password on phone, password on specific application, install Find My iPhone Application
- Compartimentalise: Use different messenger apps for different social networks, use different browsers, use a new email account as your primary email account (which is not your normal email account)  
- Obfuscate: use a VPN, change the name on your phone, put fake phone numbers in your phone. Leave you phone at home to hide where you are going but also break the pattern of your daily routine.

### Step 5: Wrap up
- See if anything is unclear, and answer questions
- Direct participants to resources
- Hand out Tactical Tech's Pocket Privacy guide for Mobiles if you have them.



-------------------------------
<!---
curriculum-training/Exhibition/TEMPLATE
-->
