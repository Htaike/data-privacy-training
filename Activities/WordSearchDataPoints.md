# Word Search: Data Traces
--- meta
title: Word Search - Data Traces
uuid: 2912c11b-1327-4c65-972a-5d01a2dfe0c1
lan: en
author: Tactical Tech
item: Activity
tags:
  - Social media
  - Choosing tools
duration: 30
description: This activity follows on from the Activity called "Word Search". See what data traces are tied to each of the products and services you've identified. What insights might a company get into your life, habits and social networks?
dependencies: "@[snippet](b2027340-48e9-42c6-803c-62fb75fe6d37)"
---

## Meta information

#### Description
This activity follows on from the Activity called "Word Search". See what data traces are tied to each of the products and services you've identified. What insights might a company get into your life, habits and social networks?

#### Duration
30 minutes

#### Ideal Number of Participants
Can be done with any number of participants.

#### Objectives
###### Knowledge
- Gain insight into how many, and what types, of data traces you leave when you use the various products and services of a big company like Google or Facebook.
- See what all these data traces taken together might look like, in terms of a detailed profile.


#### Materials and Equipment Needed
--- meta
uuid: 150efa1d-6bc0-436e-ab3a-1f153ae0a15e
tag: materials activity
---

- @[material](9793b1a6-5b29-4831-b7b7-05fcc5d31891)
- @[material](f2afe09c-14df-4e54-9e45-ed8062e4ef78)

## Activity: Word Search - "Data Traces"
--- meta
uuid: afa7b0e5-e772-4523-a01b-381fb5d16e49
---

##### Preparation
- Make sure participants have done the  Word Search activity beforehand.
- Map the different data traces related to different products/services of the company you're focusing on.

##### Map out all the data traces
- Introduce the activity.
- Break the participants into small groups of 3-4. For each of the services they found in the Word Search, they should write down:
  1. What is this product/service used for? (e.g. communication, news, posting photos/videos, connectivity etc)
  2. What data traces are left through using this product/service?

- Once their lists are complete, ask them to discuss: What sort of profile can this company create, if they connect up all these traces?

##### Feedback and discussion
Bring the group back together. Stimulate a discussion by asking the following questions: - What does it mean to use many services owned by the same company?
- What do you think it means to  compartmentalise your data traces? Why might this be a good thing to do? (talk about access, usage, etc)

##### Wrap up
To sum up: Companies like Facebook and Google own a lot of different products/services which serve a lot of different needs (photo sharing, communication, news etc). This, however, means that the company can piece together a fairly detailed overall picture of you.


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
