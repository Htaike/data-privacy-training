# How Mobile Communication Works
--- meta
title: How Mobile Communication Works
uuid: d89cab05-b823-4884-9377-071125ce51e2
lan: en
author: Tactical Tech
item: Activity
tags:
  - Mobile
  - Browser
  - Choosing tools
  - Drawing exercise
duration: 45
description: See how mobile communication works, and what information third parties have access to along the way.
---

## Meta information

#### Description
See how mobile communication works, and what information third parties have access to along the way.


#### Duration
45 - 60 minutes.


#### Ideal Number of Participants
This session can be done with any number of participants.


#### Objectives
###### Knowledge
- Understand the infrastructure of mobile communication, and who owns which parts of this infrastructure
- See where vulnerabilities lie: who can, or could, access your communication or data at which points?
- Understand the differences between cellular networks (including 3G/4G) and Wi-Fi

###### Attitude
- Mobile communication infrastructure is a physical thing, and there are choices you can make about how securely your data travels through it.


#### Materials and Equipment Needed
--- meta
uuid: 39488e0a-3687-4fa9-b074-099f786898e8
tag: materials activity
---
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)
- @[material](16c01d17-9ba7-47d6-815a-75cf9633004f)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](1d01a081-f4a9-408b-b06b-78ee6288d1ce)


#### References
Diagrams coming soon:
https://mayshadow.org/materials

## Activity: "How Mobile Communication Works"
--- meta
uuid: 541f7a7b-e83f-4018-8646-b86fcbccca4
---
##### Preparation
- Choose whether you are going to explain 'How mobile communication works' using (1) the _Mobile Communication Cards_ or using (2) paper and pen.
- For either option, be prepared to demonstrate how mobile communication works in at least two scenarios:
  - through cellular networks (voice/sms, chat apps)
  - through Wi-Fi (eg chat apps or browsing)
- If not already covered, also be prepared to draw the infrastructure of the internet including how it works when using a VPN, proxy, and Tor. It could  be useful to have a diagram at hand as a guide.
- *Cards*: If you're running the Cards version of this activity, print out ready-made cards from https://myshadow.org/materials, or create your own. You will need at least two sets: Mobile Infrastructure cards, and Internet Infrastructure cards. If the group if big, you may need to print out multiple sets. Ideally there should be one card per person.
- *Paper and pen:* Make sure you have enough paper, pens and markers. There should be multiple pieces of paper per participant.

#### (1) CARDS (40 min)

##### What does the cellular network infrastructure look like?
- Depending on numbers, this exercise can be done in one group or multiple groups. Give each group one set of cards and ask them to put the cards in the correct order, showing how a mobile phone connects to another mobile phone through a cellular network.
- Compare results and then go through the order together. Ask participants if there are specific concepts that are unclear, and clarify.

##### How does a mobile phone connect to the internet?
Using the cards, get participants to demonstrate the difference between (1) connecting to the internet through a cellular network - cell phone towers, 3G, 4G - and (2) connecting through Wi-Fi.
- What parts of the mobile phone and mobile communications infrastructure are involved?
- Who has, or could have, access to what information, at which points along the way?


#### (2) PAPER AND PEN (40 min)
##### Draw the cellular network infrastructure
- Hand out paper and pens to each participant, and write up some key words on the flip chart: cell phone tower, 3G/4G, telco, triangulation.
- Ask participants to draw what the mobile communications infrastructure looks like, showing how a text message (SMS) travels from their phone to a friend's phone.
- Break participants into small groups. to present their drawings to each other and discuss differences and similarities.
- On the flipchart, draw how how mobile communication works. This should include the baseband, 3G/4G, Cell phone towers, mobile phone provider, triangulation.

##### Draw how a mobile phone connects to the internet
- Ask participants to get into small groups and together draw how a mobile phone connects to the internet (give a specific case study, eg 'connecting to a website')
- Groups come back together, and compare their drawings.
- On the flipchart, draw two ways in which a mobile phone can connect to the internet:
  - through the baseband, 3G/4G, Cell phone towers
  - Through Wi-Fi Receiver, Router, internet provider, internet infrastructure.
- Discuss: Who has, or could have, access to what information, at which points along the way?

#### Key concepts to cover
- Mobile phone infrastructure and triangulation
- Internet infrastructure
- The difference between connecting over 3G/4G (cellular networks) and Wifi (internet).


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
