# Alternatives
--- meta
title: Alternatives
uuid: f87ee79d-03ba-4714-b039-dc4346432a8c
lan: en
author: Tactical Tech
item: Activity
tags:
  - Choosing tools
  - Mobile
  - Secure messaging
  - Browser
duration: 30
description: How are commercial and non-commercial tools and services different? Why does it matter? This activity offers a simple framework for making informed choices, and introduces some alternative options.
---

## Meta information

#### Description
How are commercial and non-commercial tools and services different? Why does it matter? This activity offers a simple framework for making informed choices, and introduces some alternative options.

#### Duration
35 minutes


#### Ideal Number of Participants
Minimum of 4.


#### Objectives
###### Knowledge
- Gain a framework to evaluate tools and services.
- Increase knowledge on open source, and ownership of tools and services.

###### Skills
- Use the given framework to evaluate a selection of key tools.

###### Attitude
- As a user you have to choose what information you want to protect. Do you want to manage your identity, social network, content or location? Protecting one might expose the other.


#### Materials and Equipment Needed
- @[material](6ebfc3d0-3f8a-41b9-800b-5e802b985fd8)

#### References
[Alternatives](https://myshadow.org/increase-your-privacy#alternatives), Me and My Shadow (Tactical Tech)


## Activity: "Alternatives"
--- meta
uuid: bc9d5aee-0106-41dd-9164-7f2157e16260
---
##### Preparation
- Print a blank Alternatives grid for each participant.
- Have a filled-in grid ready with the type of services you will be focusing on (search engines, messaging apps, etc).

##### Brainstorm apps and tools (5 min)
 - Briefly introduce the activity, and give each participant a blank Alternatives workbook page.
 - Focusing on a specific type of service (search engines, messaging apps, etc), put participants into pairs and ask them to fill in the first column with the names of relevant services (eg messaging apps: Whatsapp, Snapchat, Signal)

##### Evaluation framework (10 min)
- Using one example, (eg Whatsapp) walk the group through each column step by step, explaining concepts and answering questions as they arise.

##### Fill in the grid (10 min)
- Get participants back into pairs and give them time to fill in as much of the grid as they can.
- Give them a filled-in grid to compare their own against, and answer any questions.

##### Managing information (10 min)
- Introduce a framework for thinking about the information you need to manage. This can be broken down into four areas: identity, social networks, content and location.
- When choosing a tool, it's important to think about what data you want to "protect". For example, a tool might protect your content with encryption, but might also require access to specific information like your phone number, making it impossible to use as psuedonymously or anonymously. If pseudonymity/anonymity is what you need, then a different tool might suit your needs better.


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
