# Bus Stop
--- meta
title: Bus Stop
uuid: 7b6d19c8-1b71-4bf6-8425-808a1059dda3
lan: en
author: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
duration: 15
description: Find out what others in the group think about a variety of connected ideas, topics, or questions.
---

## Meta information

#### Description
Find out what others in the group think about a variety of connected ideas, topics, or questions.


#### Duration
15 minutes


#### Ideal Number of Participants
Minimum of 6.


#### Objectives
###### Knowledge
- Get a wider view on a specific topic by learning what the others in the group think.

#### Materials and Equipment Needed
--- meta
uuid: e74a1559-b745-4a63-88e6-e3028b07e12c
tag: materials activity
---
@[material](417fc8a5-400f-4553-a23e-faa949beb239)
@[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
@[material](c0358b51-fe16-47ae-9686-927ec39d18f6)


## Activity: "Bus Stop"
--- meta
uuid: 056617f0-88f0-4bd6-aa25-50b1150b48db
---
##### Preparation
- Have at least 3 pre-prepared topics/questions (that are connected somehow) that you want the group to think about.
- Write each question/topic on the top of a large sheet of paper and hang these up on the wall in different parts of the room. Each paper represents one "bus stop". There should be a minimum of 3 stops - the number will depend on the number of participants.


##### Start the bus
- If there are 4 bus stops, split participants into 4 groups. If the number of participants is very large, make more bus stops. Ask each group to stand at a different bus stop.  
- Each group has 2-3 minutes at their bus stop to write down as many words, terms, phrases and drawings they can think of in relation to the topic/question written at the top of the paper.
- After 2-3 minutes the bus comes, and each group has to rotate one bus stop to the right. They then do the same with the new topic/question, adding to what's already there.
- Continue rotating until all the groups have been to all bus stops.


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
