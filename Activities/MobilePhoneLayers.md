# Your mobile phone: a breakdown in four layers
--- meta
title: Your mobile phone - A breakdown in 4 layers
uuid: 969fbfdb-3a2a-4459-9533-2e05c00c0198
lan: en
author: Tactical Tech
item: Activity
tags:
  - Mobile
duration: 40
description: COME BACK TO THIS ACTIVITY Break down a mobile phone into four layers - core, sensors, operating system and applications, and data traces.
---

## Meta information

#### Description
Our mobile phones - powerful little computers that we carry around with us, and use every day - are increasingly crucial to our lives; and yet we know surprisingly little about how it.

This exercise breaks down the mobile phone into four layers: the phone's core, sensors, operating system and applications, and data traces. This lays a foundation for participants to see which companies and institutions have access to which parts of their phone; what data these companies can collect; and what choices they can make make in terms of how much and what data is shared.


#### Duration
30-45 minutes


#### Ideal Number of Participants
This activity can be done with any number of participants.


#### Objectives

###### Knowledge
- Understand the basics of how a mobile phone works, in terms of both hardware and software.
- Understand how different parts of the mobile phone are used for information collection and tracking.

###### Attitude
- Mobile phones are generally insecure devices.
- Protecting the privacy of yourself and others involves making choices about how you use your phone.

#### Materials and Equipment Needed
--- meta
uuid: 05258258-b2bd-4174-bf17-fe994930e5f1
tag: materials activity
---

- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](a1108a3a-7bd8-4afc-9255-02a181ccffef)


#### References
- What is your mobile phone, Me and My Shadow
- Location Tracking, Me and My Shadow https://myshadow.org/location-tracking


## Activity: "Your Mobile Phone - A breakdown in 4 Layers"
--- meta
uuid: be34b4fa-dbc9-4b15-aa3e-b9354ddf5ee8
---

##### Preparation
- Read the _Mobiles Reference Document_.
- Download the pdf "What is a Mobile Phone?"
- Either print out the pdf to distribute (one copy per 4-6 participants) or have the pdf ready to project onto a screen.


#### What's a mobile phone?
From the group, elicit components of a mobile phone and write them up on a flipchart. Possible answers could include:

- keyboard
- screen
- antennae
- SIM card
- microphone
- speaker
- microphone
- battery
- baseband

_Note:_ If participants hesitate, ask targeted questions such as 'How does the phone record your voice?' (It has a microphone) - How does the phone store your contacts?'' (It has memory, like a PC hard drive).

###### Introduce the mobile phone in 4 layers
Walk the group through each page of the pdf and ask if any terms or aspects of the mobile phone are unclear.

###### Focus on one layer
- Break participants into four groups, and give each group one page of the pdf. ("Core", "Smart", "Operating System", "Data Traces").  

- Set the following questions for group discussion (_note:_ each group only needs to answer the questions relevant to their own page):

   1. What data is created? 
   2. Who has access to this data?
   3. What are different actions you can take to increase your privacy and security?

###### Feedback
Bring the group together for feedback, clarifying anything that's unclear.

###### Data traces
Ask particpants to get back into the same groups and set the following questions:

- _For the groups with "Core", "Smart", and "Operating System"_: Focusing on the items on your page, what can you do to increase your privacy and get more control over your mobile phone?

- _For the group with "Data Traces"_: What are the risks involved in creating and sharing these data traces? (Also think about these traces being connected together)

###### Feedback
Each group presents their findings. After each presentation, ask the other groups if there is anything they can add, and offer clarification/information where needed.

#### Key points to cover:
MOVE TO MOBILES REFERENCE Document
### Page 1: "Core"
- **IMEI Baseband:** The Baseband is what makes connection between the mobile phone provider and the mobile infrastructure possible. The IMEI is the unique number of the phone, and is connected to the baseband. In most countries it is illegal to change the IMEI number, and in fact on most mobile phones this is not even possible.

- **Battery:** Is the battery removable? This is the only way to really turn a mobile off.

- **Central Processing Unit (CPU):**
Note - more and more phones integrate the Baseband into the CPU, instead of keeping them separate. Integration means that the mobile phone provider could get access to the CPU and the CPU could have access to the mobile infrastructure (cell phone towers and location tracking).

###### Page 2: "Smart"
- **GPS:** There are three ways to determine the location of a mobile phone: Through the baseband, via cell towers (using triangulation to measure exact location); through GPS, via satellites; and through Wi-Fi.

- **Wi-Fi:** When Wi-Fi is switched on, your phone will broadcast known Wi-Fi networks, offering a history of the networks your mobile has connected to in the past. This could also be used to predict your future location.

- **SIM Card:** The SIM card holds your phone number. In many countries, you need to register for your phone number with your legal identity, either when you buy the SIM, or after a few weeks.

- **Position sensors:** Is the phone lying down or held upright? Is it accelerating? Moving in a specific direction? Position sensors can be used to interpret whether someone is moving, calling, texting, or sleeping.

###### Page 3: "Operating System"
- **Operating System:** Each operating system is developed and owned by a different company (Android - Google, iOS - Apple, Windows - Microsoft), and has pros and cons from a privacy/security perspective.  _More information can be found in the Reference Document_.

- **Built-in apps:** These come with your phone and you can't delete them. New built-in apps can also be included in updates. For Android phones, built-in apps can come not only from Google, but also from the manufacturer (Sony, Samsung etc).

- **Third Party Apps:** Apps you install voluntarily from third parties

- **App Store:** App store come with your operating system. Play Store for Android, App Store for iPhone. Both App Stores need a email address to work and collect all your installations and purchases. Note that if an existing email address is associated with a credit card, a phone that gets tied to this email address gets tied to the known history and previous financial transactions . For Android there is an alternative App Stores, F-Droid, which does not require any information from the user.

- **Note:** The above does not necessarily apply to a phone that has been rooted. More information about rooting can be found in the _Mobiles Reference Document_.

###### Page 4: Data Traces
- **Financial information:** Most app stores are connected to a credit card, which is connected to a name and bank account.

- **Email:** Which primary email accounts is tied to tied to the app store? Your email provider will receive notification on every app store purchase that you make. For example if you have an iPhone and you are using Gmail, you share information with both companies. Gmail is able to tie your AppStore activity to your profile.

- **Timestamps and frequency:** Timestamps and frequency of contact can be used to analyse mobile data in bulk (using powerful computer programs), to reveal patterns of your networks, social relationships, location, and so on.

For example, when you call someone: is the call happening during the day or at night? For how long? How often do you call this person? This can reveal whether they are a partner, friend, colleague or family.

When this information is combined with other data traces, and with the data traces of other people, the picture can become incredibly detailed.  


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
