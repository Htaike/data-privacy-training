# Visualising Tracking in the Browser
--- meta
title: Visualising Tracking in the Browser
uuid: b6953dff-a5bc-4f97-af37-57f05a35817b
lan: en
author: Tactical Tech
item: Activity
tags:
  - Mobile
  - Browser
duration: 30
description: See how tracking works as you browse the internet. Who is collecting information about you?
dependencies: "@[snippet](e0d34b63-d015-4338-9081-7ac67f26da92)"
---

## Meta information

#### Description
See how tracking works as you browse the internet. Who is collecting information about you?


#### Duration
30 minutes


#### Ideal Number of Participants
Minumum of 4 participants.


#### Objectives
###### Knowledge
- See how tracking in the browser works, and learn about trace-routes: how data travels across the internet.
- Understand what types of data are being collected, and by who; what is meant by profiling; how companies can track individuals across websites; and what browser fingerprinting is.


#### Materials and Equipment Needed
--- meta
uuid: fadf8562-b12f-41eb-8019-83c33d0e7bae
tag: materials activity
---
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)


#### References
- REFERENCE DOCUMENT FOR BROWSERS
- [Trackography](https://trackography.org) (Tactical Tech)
- [Lightbeam](https://myshadow.org/resources/lightbeam?locale=en)
- [Panopticlick](https://myshadow.org/resources/panopticlick?locale=en), (EFF)


## Activity: "Visualising tracking in the browser"
--- meta
uuid: ea61792c-218f-48a0-8a9c-47fa611f07d3
---

##### Preparation
- Make sure you are familiar with Trackography and Lightbeam. (More info can be found in the Reference Document: Browsers)
- Make sure you have enough computers available (1 per small group). Note that  Lightbeam can't be installed on a mobile phone, and Trackography only works on larger screen.
- Write up links for Trackography website and Lightbeam, or project these onto a screen.
- If there is limited internet, you will need to download the Trackography movie and screen-cast Lightbeam.

##### Explore Trackography & Lightbeam
- Divide participants between Trackography and Lightbeam, breaking into smaller groups if necessary.
- Participants should explore the two tools, and discuss their findings.

##### Feedback and discussion
Groups report back on what was discovered. Fill in gaps and offer explanations as needed.

The following concepts should be covered:
- What is tracking?
- What type of data is being collected, and by whom?
- What is Profiling?
- How can companies track me across website? What is browser fingerprinting? (Demo EFF's Panopticlick.)
- Trace-routes: how data travels across the internet.
- Go over how the internet works, if needed.


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
