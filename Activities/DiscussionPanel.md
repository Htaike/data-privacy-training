# Panel discussions
--- meta
title: Lager House Discussions
uuid: 4b1b0121-4ec5-4598-a439-14e3d8091859
lan: en
author: Tactical Tech
original source: Privacy International
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Role-play exercise
  - Debate
duration: 60
description: Role-play a panel discussion - Bring to light arguments from multiple viewpoints and test out possible responses.
---

## Meta information

#### Description
Role-play a panel discussion: Bring to light arguments from multiple viewpoints and test out possible responses.

#### Duration
40 - 60 min


#### Ideal Number of Participants
Minimun of 4.

#### Objectives
###### Knowledge
- Understand some common arguments from various actors in the data environment.

###### Skills
- Gain confidence in expressing and rebutting arguments around a specific issue.

## Activity: "Discussion Panel"
--- meta
uuid: bab9661c-edb0-42c6-bfa0-6481e98d7038
---
##### Preparation
- Write down a scenario to be discussed (e.g. a law has been passed requiring communications companies to collect all its users' metadata, store it for 3 years, and pass it to relevant local authorities when requested).
- Identify 4 characters who would be involved in, or affected by, this scenario (eg Privacy Activist, Law Enforcement, Telecommunications Company or Politician.).
- For each character, write down a 'cheat-sheet' with a description of the role and some key arguments they might present.  
- Prepare a sheet for the moderator listing some specific question that will help them guide the discussion, as well as the following instructions:
 - Kick-start the panel by asking everyone to introduce themselves.
 - Try to keep the conversation flowing, throw in some challenging questions, and ensure that each participant gets a chance to speak.

##### Set-up and demo (5 min)
 - Ask for 4 volunteers to be on the panel, and one volunteer to be the moderator.
 - Give each panelist the cheat-sheet with their role and arguments on it. Give them a few minutes to read everything.

##### Panel discussion (20-25 min)
Ask the moderator to kick off the panel.

##### Feedback and follow-up discussion (20-30 min)
- After the panel discussion has finished, have a feedback conversation with the larger group. How was the exercise? Was there anything interesting/unexpected? Was anything important left out?   

-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
