# Draw Your Browser History
--- meta
title:
uuid: b6be8eed-7382-4594-bbe1-eaf471f8f081
lan: en
author: Tactical Tech
item: Activity
tags:
  - Drawing exercise
  - Browser
  - Mobile
duration: 45 - 65 min
description:
---

## Meta information
#### Description
Get a better sense of how you use your browser, and what types of data traces are created in the process.

#### Duration
45 - 65 minutes


#### Ideal Number of Participants
This activity can be done with any number of participants.


#### Objectives

###### Knowledge
- Gain a clearer understanding of how you create digital traces through your browser
- Know what is meant by the terms 'content' and 'metadata', and how they are different.

#### Materials and Equipment Needed
- @[material](e664e54e-f6cb-4e88-bc1e-cbdfed060456)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)

#### References
Browser Reference Document

## Activity: "Draw Your Browser History"
--- meta
uuid: ce5cd1dc-3732-4156-90ed-8503177a871f
---

##### Preparation
- Print out some Cartoon templates - at least one per person. The Templates come in three options; try and print out at least two of these variations, and then let participants choose.
- Have the templates and coloured pens available on a materials table.

##### Drawing (15 min)
- Participants can fetch one or more comic templates, as well as pens and paper, from the materials table.  
- Ask participants to  draw their browser history of the day or days before.

##### Compare drawings (15 min)
Get participants into small groups of two to four to present their drawings to each other, and discuss:
- What are similarities, differences or things that stand out?
- Bring the group back together and have the small groups report back.

##### Digital traces (15 min)
Lead the group through a discussion on data traces focusing on content and metadata.
- What is meant by the terms 'content and metadata'? How are they different?
- Make two column on the board: Content, and Metadata.
- Which types of digital traces are left through the browser? Put these up on the board in the two columns.

##### (Optional) How much control do we have over our data? (15 mins)
Introduce Bruce Schneier's 'levels of control' framework.



-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
