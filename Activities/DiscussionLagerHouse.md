# Lager House Discussions
--- meta
title: Lager House Discussions
uuid: 93d24b85-619f-45dc-a108-b48f1da26431
lan: en
author: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Debate
duration: 60
description: Interrogate and practise arguments around a controversial topic.
---

## Meta information

#### Description
- Interrogate and practise arguments around a controversial topic.


#### Duration
30 - 60 min


#### Ideal Number of Participants
Minimum of 8

#### Objectives
###### Knowledge
- Interrogate your own, and opposing, positions on a particular subject.


###### Skills
- Be able to argue for a specific point if view
- Be able to anticipate and rebut arguments leveraged against your own position.

#### Materials and Equipment Needed
--- meta
uuid: 52e84794-467d-40d4-a327-06da5f79f29e
tag: materials activity
---
Chairs to sit on (one per participant), or if no chairs available, tape to put on the floor and divide the space.


## Activity: "Lager House Discussion"
--- meta
uuid: 6d8b622b-de50-4dc6-8696-1b2bfde78dec
---
##### Preparation
- Prepare a list of controversial statements e.g. "If you have nothing to hide, there's nothing to fear." "When you are trolled the only effective response is to troll back."

- Set up the space with two rows of chairs facing each other, or if there are no chairs, split the room in two with a line on the floor (people will need to sit on the floor, or stand).


##### Set-up and demo (5 min)
- Read the first statement out loud.
- Ask for two volunteer debaters: one will argue in favour of the statement, the other against. For now, have each debater stand at the top end of a row of chairs.
- Ask the rest of the participants to choose whether they agree or disagree with the statement, and to sit in the corresponding row of chairs.

##### Debate and follow-up discussion (25-55 min)
- The two debaters at the front take turns making arguments. After each argument, supporters can switch sides when they find an opposing argument convincing.
- After some initial debate, the supporters are invited to join in the discussion as well.


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
