# HTTPS and Tor
--- meta
title: HTTPS and Tor
uuid: 2c53a124-eb4f-4dec-a6dc-e3d475b82622
lan: en
author: Tactical Tech
original source: Electronic Frontier Foundation
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
duration: 30
description: CHECK THIS AGAIN TO FIT IN WITH VISUALISING THE INTERNET ACTIVITY. What information can third parties see when you browse the web or send email? How does it make a difference when you use HTTPS or Tor?
---

## Meta information

#### Description
What information can third parties see when you browse the internet or use webmail? How does it make a difference when you use HTTPS or Tor?

#### Duration
30 minutes


#### Ideal Number of Participants
Minumum of 4 participants.


#### Objectives
###### Knowledge
- See what information third parties can see when you browse the internet or use webmail
- Understand the difference between HTTP and HTTPS
- Know how Tor works.


#### Materials and Equipment Needed
---
uuid: e8720aeb-6671-4c4a-a0fe-a9588618529d
tag: materials activity
---

- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)


#### References
- - EFF diagram: When you use the internet, who can see what information and at which points along the way? Includes different scenarios: VPN, Tor, Proxy, and HTTPS. https://www.eff.org/pages/tor-and-https


## Activity: "Https and Tor"
--- meta
uuid: 9a32d927-f292-4834-939a-320fa96df94d
---

#### Preparation
- If some participants have computers (one per group), write the URL up: https://www.eff.org/pages/tor-and-https
- Alternatively, print out the diagram in each 'mode' (HTTP, HTTPS, Tor) - one set for each group.

#### Explore HTTPS and Tor
- Break participants into small groups  
- Give the groups some time to explore the 'Tor and HTTPS' diagram, and let them discuss their findings.

##### Feedback and discussion
Groups report back on their findings. Discussion should cover:
 - Who has access to your data traces?
 - What is the difference between HTTP and HTTPS?
 - What is Tor?
 - How does Tor anonymise your browsing and block online tracking?

-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
