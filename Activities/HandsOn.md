# Hands-on Sessions
--- meta
title: Hands-on sessions
uuid: 844ed57d-c7b6-428c-bd23-1a39983b0a93
lan: en
author: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Hands-on
duration:
description: How to run a hands-on session
---

## Meta information

#### Description
How to run a hands-on session: Take participants through practical ways to increase control.

#### Ideal Number of Participants
The ideal number of participants for a Hands-on session depends on the number of facilitators available.

There should always be a minimum of two facilitators, and if the session splits participants into groups according to their operating systems, for example iOS or Android, there should be a mimimum of one facilitator per group, with an extra facilitator to step in where needed (see _How-To Organise a Training_).

- 2-20 participants: 2-3 trainers
- 20-28 participants: 4 trainers

#### Objectives
###### Skills
- This depends on the objectives of the session, but will generally involve being able to install and use a certain tool, or change settings within a specific tool or service.


#### Materials and Equipment Needed
--- meta
uuid: 7465b885-9b8a-44e5-aab4-7e2b6e682846
tag: materials activity
---
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
-  @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)

#### How to run a Hands-On Session
--- meta
uuid: ab6c7bf6-330c-428b-99c8-03aad4552ddf
---

##### Preparation
- Test out all the tools and settings that relate to the topic you want to cover.
- Find or create resources to help participants self-learn along the way.


##### Steps
- Break the group according to the size, number of trainers and, where appropriate, operating systems (eg Android or iPhone). Each group should have at least one trainer.
- Walk each group through the steps involved, in an interactive way, with step-by-step instructions or guidance projected on the wall or printed out.


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
