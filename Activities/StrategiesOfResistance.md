# Strategies of Resistance
--- meta
title: Strategies of Resistance
uuid: fc709e80-4eca-4185-927a-7ea3b0d13659
lan: en
author: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
duration: 30
description: COME BACK TO THIS ACTIVITY Four broad strategies to play around with your digital shadow.
---
## Meta Information

#### Description
Four broad strategies to play around with your digital shadow.

#### Duration
30 minutes

#### Ideal Number of Participants
Minimum of 9 participants.


#### Objectives
###### Knowledge
- Gain insight into different strategies for playing with your data.
- Understand the benefits and limitations of each strategy.

###### Skills
- Be able to use some of the strategies discussed.


#### Materials and Equipment Needed
--- meta
uuid: 64775c4e-b5d3-423f-85b1-2d3ee0fab9ca
tag: materials activity
---

- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)


#### References
Strategies of Resistance:

**Reduction**

Less is more! Data that is not created can not be collected, analysed, stored nor sold. Therefore, the reduction strategy is to reduce the amount of data we produce the better.

Examples:

1. Limit data generation by resisting to give information, you do not need to fill out all the field in registration forms.
2. Clean your online identity, delete the apps from your mobile phone you no longer use, and erase pictures, emails and messages that are outdated.
3. Block unwanted access, install Privacy Badger and NoScript to block cookies and other third party scripts from running in your browser and collecting data.

**Obfuscation**

Hide in the crowd! Confuse companies with noise. The obfuscation strategy is to create a lot of fake information so that companies, governments or other individuals do not understand what data is true and what data is false.  

Examples:
1. Hide in the crowd by creating several fake social media profiles with similar names or similar pictures.
2. Mask the identity of you and your group by creating a Facebook account that represents you as a group
3. Create noise by clicking on random adds or install Adnoisium, a tool that will click on random advertisment for you.
4. Mislead Google by installing TrackMeNow that will generate random search queries, masking your real searches and questions.
5. Break your routines ....

**Compartmentalisation**

Diversifaction rules! separate your actions, create multiple identities. Offline we have different characters depending on our social situation. The compartimentalisation strategy is to separate information so that there are different versions of us on the internet.

Examples:
1. Map your identity by searching for your name and writing down the different accounts you have. This is the first step to understand how you can separate your identity.
2. Devide your identity between social media services, by creating different accounts with different names
3. Separate information;, use different browsers for different internet activities, use different messenger apps for different friend and family circles.
4. Isolate valuable or personal data by storing it on a different device
5. Disassociate your work life from your social life by using different email accounts.

Notes: Separate your different social networks, interest and identities. Offline people are wizzards in managing multiple identities. At work or school you might be a different version of yourself then at home, in the bar you might be different from when you are at the gym. Yet, online you are expected to have just one identity.

**Fortification**

My devices, my bills, my rules! Create barriers, restrict access and visibility. This strategy is to keep your data safe from prying eyes.

Examples:

1. Create a barrier, use an anti virus software and keep it up to data.
2. Keep your data under lock and key, encrypt your mobile, computer and tablet.
3. Break all signals, turn off Wi-Fi and bluetooth when not in use and put your phone in faraday bag when you don't want to be followed
4. Simple but effective, cover your webcam when not in use
5. Create a secure internet connection, using HTTPS Everywhere in the browser


## Activity: "Strategies of Resistance"
--- meta
uuid: 1667798e-fb3d-4ae6-a744-ddc6ae44bdf8
---

##### Preparation
- Prepare to present the four categories of resistance. Have a full list of examples for each one.
- Decide which area you would like the group to focus on (for example browser tracking, location tracking, mobile phones in general, etc).

##### Four types of resistance (15 min)
NEED TO FILL THIS IN MORE AND PROVIDE EXAMPLES.
- Ask participants for a few ways they have tried to increase their privacy online. Put some of these on the board.
- Use the examples on the board to lay out four broad strategies of resistance:

  1. Reduction (Reduce)
  2. Obfuscation (Confuse/Create noise)
  3. Compartimentalisation (Separate)
  4. Fortification (Fortify)

##### Brainstorm strategies  (15 min)
- Split participants into four groups, and give each group one of the three strategies: Reduction, Obfuscation, Compartmentalisation, Fortification.
- Set the focus area.
- Each group should brainstorm ways in which they can reduce / obfuscate / compartimentalise /fortify their data in this area.

##### Feedback: presentations  (15 min)
- Each group reports back to the entire group in a 2-3 minute presentation.

##### Discussion (10 min)
- Lead a brief discussion on the benefits and limitations of each strategy, feeding in where necessary.


-------------------------------
<!---
curriculum-training/Activities/TEMPLATE
-->
