# Materials

## Computer
--- meta
uuid: 0d1c2469-bc55-41da-8207-63edf8fd307b
tags:
  - material
---

## Beamer
--- meta
uuid: ce457811-1423-4ff0-93bb-7bc2fda1e844
tags:
  - material
---

## Participant's computers
--- meta
uuid: e96c589f-f1c5-49de-8493-ca39de05a502
tags:
  - material
---

## Participant's phones
--- meta
uuid: 6d758ada-e6cf-4a56-a96b-f84dfe14181c
tags:
  - material
---

## Internet connection
--- meta
uuid: f6cd74bf-5a89-4fdc-8122-e305f947e14c
tags:
  - material
---

## Post-its
--- meta
uuid: c0358b51-fe16-47ae-9686-927ec39d18f6
tags:
  - material
---

## Flip chart
--- meta
uuid: 417fc8a5-400f-4553-a23e-faa949beb239
tags:
  - material
---

## Markers
--- meta
uuid: 9392dacf-999c-4c33-a6d8-4545c1aee849
tags:
  - material
---

## Workshop materials
--- meta
uuid: 67c7149b-44f9-4b7a-b4ee-c8bf786b50dc
tags:
  - material
---

## Envelopes
--- meta
uuid: 2c06216d-1383-4ec0-9048-5c5cc51cdde0
tags:
  - material
---

## Mobile phone breakdown in 4 layers
--- meta
uuid: a1108a3a-7bd8-4afc-9255-02a181ccffef
tags:
  - material
---

## How the internet works - cards
--- meta
uuid: 1d01a081-f4a9-408b-b06b-78ee6288d1ce
tags:
  - material
---

## Alternatives grid
--- meta
uuid: 6ebfc3d0-3f8a-41b9-800b-5e802b985fd8
tags:
  - material
---

## Communication Model - workbook
--- meta
uuid: b54e63a1-5ea6-4337-bb16-efc4fcc7b95c
tags:
  - material
---

## Browser - workbook
--- meta
uuid: c1b99fb2-eeda-4c25-8c7a-3e882a828c5e
tags:
  - material
---

## Location tracking - workbook
--- meta
uuid: ec71a937-d4df-472c-9a95-a388f70940dd
tags:
  - material
---

##  Facebook
--- meta
uuid: 9793b1a6-5b29-4831-b7b7-05fcc5d31891
tags:
  - material
---

- [Word Search](https://myshadow.org/ckeditor_assets/attachments/97/facebook_wordsearch.pdf)
- [Answer Sheet](https://myshadow.org/ckeditor_assets/attachments/98/facebook_wordsearch_cheatsheet.pdf)


## Google
--- meta
uuid: f2afe09c-14df-4e54-9e45-ed8062e4ef78
tags:
  - material
---

- [Word Search](https://myshadow.org/ckeditor_assets/attachments/99/google_wordsearch.pdf)
- [Answer Sheet](https://myshadow.org/ckeditor_assets/attachments/100/google_wordsearch_cheatsheet.pdf)

## Polariod camera
--- meta
uuid: 388e928a-47b8-4cb0-8e43-dc7ec2fc8c86
tags:
  - material
---

## Cartoon Templates
--- meta
uuid: e664e54e-f6cb-4e88-bc1e-cbdfed060456
tags:
  - material
---

## Pocket Privacy - Browser
--- meta
uuid: ce39f7f2-0357-419a-aca7-81806e00e6cf
tags:
  - material
---

## Pocket privacy - Mobile
--- meta
uuid: 9824a3dd-12ed-4684-9afa-dd75118404ee
tags:
  - material
---

## Paper (A4)
--- meta
uuid: 16c01d17-9ba7-47d6-815a-75cf9633004f
tags:
  - material
---

## Pens
--- meta
uuid: b6be8eed-7382-4594-bbe1-eaf471f8f081
tags:
  - material
---

## Exposure quiz
--- meta
uudi: 10492a69-87ad-49c7-8cd8-764815ab5da4
tags:
  - material
---
